Pod::Spec.new do |s|

  s.name         = "SmartSDK"
  s.version      = "2.0.5"
  s.summary      = "The smartest sdk in the whole world."

  s.homepage     = "http://magis.com.tr"

  s.license      = { :type => "MIT", :http => "http://magismobile.com/repos/com/oksijen/ios/smartsdk/LICENSE" }
  
  s.authors      = { "Rashid Ramazanov" => "rashid.ramazanov@magis.com.tr", "Eren Aladag" => "eren.aladag@magis.com.tr" }

  s.platform = :ios, '8.0'
  s.ios.deployment_target = '8.0'

  s.source       = { :http => "http://magismobile.com/repos/com/oksijen/ios/smartsdk/2.0.5/SmartSDK.framework.zip" }

  s.vendored_frameworks = 'SmartSDK.framework'

  s.preserve_paths = 'SmartSDK.framework'

  s.framework  = "SmartSDK"

  s.resources = "Source/smartsdk-cert.der"

  s.dependency "Realm", "2.10.1"
  s.dependency "RealmSwift", "2.10.1"

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.0.1' }

end
