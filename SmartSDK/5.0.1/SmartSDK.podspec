Pod::Spec.new do |s|

  s.name         = "SmartSDK"
  s.version      = "5.0.1"
  s.summary      = "The smartest sdk in the whole world."

  s.homepage     = "http://magis.com.tr"

  s.license      = { :type => "MIT", :http => "http://magismobile.com/repos/com/oksijen/ios/smartsdk/LICENSE" }
  
  s.authors      = { "Rashid Ramazanov" => "rashid.ramazanov@magis.com.tr", "Eren Aladag" => "eren.aladag@magis.com.tr" }

  s.platform = :ios, '8.0'
  s.ios.deployment_target = '8.0'

  s.source       = { :http => "http://magismobile.com/repos/com/oksijen/ios/smartsdk/5.0.1/SmartSDK.framework.zip" }

  s.vendored_frameworks = 'SmartSDK.framework'

  s.preserve_paths = 'SmartSDK.framework'

  s.framework  = "SmartSDK"

  s.resources = "Source/smartsdk-cert.der"

  s.dependency "Realm", "3.5.0"
  s.dependency "RealmSwift", "3.5.0"
  s.dependency "GzipSwift", "4.0.0"

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.0.1' }

end
